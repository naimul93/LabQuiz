﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabQuiz
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string item_name;
        string s;
        double amount;
        double discount_rate;
        double discount_amount;
        double netPayable;

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Form2 f2 = new Form2();
            f2.ShowDialog();
            f2.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            if (comboBox1.SelectedItem.Equals("Saree"))
            {
                item_name = comboBox1.Text.ToString();
            }
            else if (comboBox1.SelectedItem.Equals("Punjabi"))
            {
                item_name = comboBox1.Text.ToString();
            }
            else if (comboBox1.SelectedItem.Equals("Lungi"))
            {
                item_name = comboBox1.Text.ToString();
            }
            else
            {
                item_name = string.Empty;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            s = textBox1.Text;
            numericUpDown1.Enabled = true;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            amount = double.Parse(s);
            if ((item_name.Equals("Saree")) && (amount>=1800))
            {
                numericUpDown1.Enabled= true;
            }
            else if ((item_name.Equals("Punjabi")) && (amount >= 1200))
            {
                numericUpDown1.Enabled = true;
            }
            else if ((item_name.Equals("Lungi")) && (amount >= 500))
            {
                numericUpDown1.Enabled = true;
            }
            else
            {
                numericUpDown1.Enabled = false;
                numericUpDown1.Value=0;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            discount_rate =(double) numericUpDown1.Value;
            discount_amount = discount_rate / 100 * amount;
            netPayable = amount - discount_amount;
            textBox2.Text = discount_amount.ToString();
            textBox3.Text = netPayable.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (Control ctrl in Controls)
            {
                if (ctrl is TextBox)
                {
                    TextBox textBox = (TextBox)ctrl;
                    textBox.Text = null;
                }

                if (ctrl is ComboBox)
                {
                    ComboBox comboBox = (ComboBox)ctrl;
                    if (comboBox.Items.Count > 0)
                        comboBox.SelectedIndex = 0;
                }
                if(ctrl is NumericUpDown)
                {
                    numericUpDown1.Enabled = false;
                    numericUpDown1.Value = 0;
                }
            }
        }
    }
}
